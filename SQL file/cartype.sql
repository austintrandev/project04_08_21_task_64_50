-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2021 at 07:34 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp_car`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartype`
--

CREATE TABLE `cartype` (
  `id` int(11) NOT NULL,
  `type_code` varchar(255) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cartype`
--

INSERT INTO `cartype` (`id`, `type_code`, `type_name`, `car_id`) VALUES
(1, 'SC40', 'Volvo SC40', 1),
(2, 'SC60', 'Volvo SC60', 1),
(3, 'SC90', 'Volvo SC90', 1),
(4, 'GLA200', 'Mercedes GLA200', 2),
(5, 'E400', 'Mercedes E400', 2),
(6, 'GLC250', 'Mercedes GLC250', 2),
(7, 'A4', 'Audi A4', 3),
(8, 'A5', 'Audi A5', 3),
(9, 'A6', 'Audi A6', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartype`
--
ALTER TABLE `cartype`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_6vu3rkdc47vv1hwbu3jd4ou92` (`type_code`),
  ADD KEY `FK9g3ynj4y4dgmq0y1re0va2k69` (`car_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartype`
--
ALTER TABLE `cartype`
  ADD CONSTRAINT `FK9g3ynj4y4dgmq0y1re0va2k69` FOREIGN KEY (`car_id`) REFERENCES `car` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
